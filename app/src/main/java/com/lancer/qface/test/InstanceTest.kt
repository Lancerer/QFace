package com.lancer.qface.test

/**
 *Time:2019/8/1
 *Author:toyk1hz1
 *Des:
 */
class InstanceTest {


    companion object {
        //方式二
//        val instance by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
//            InstanceTest()
//        }

        //静态内部类
        fun getInstance() = Holder.instance
    }

    private object Holder {
        val instance = InstanceTest()
    }


}

//kotlin中单例的写法，第一种是直接在类名前加上object关键字类似于静态内部类的方法的单例object InstanceTest{}
