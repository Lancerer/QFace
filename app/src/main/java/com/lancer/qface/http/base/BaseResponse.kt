
package com.lancer.qface.http.base


data class BaseResponse<T>(
    val errorMsg: String,
    val errorCode: Int,
    val data: T
)


