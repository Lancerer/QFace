package com.lancer.qface.http.bean

/**
 * 面部识别结果的bean
 * @author chaochaowu
 */
class FaceppBean {
    /**
     * image_id : Dd2xUw9S/7yjr0oDHHSL/Q==
     * request_id : 1470472868,dacf2ff1-ea45-4842-9c07-6e8418cea78b
     * time_used : 752
     * faces : [{"landmark":{"mouth_upper_lip_left_contour2":{"y":185,"x":146},"contour_chin":{"y":231,"x":137},"right_eye_pupil":{"y":146,"x":205},"mouth_upper_lip_bottom":{"y":195,"x":159}},"attributes":{"gender":{"value":"Female"},"age":{"value":21},"glass":{"value":"None"},"headpose":{"yaw_angle":-26.625063,"pitch_angle":12.921974,"roll_angle":22.814377},"smile":{"threshold":30.1,"value":2.566890001296997}},"face_rectangle":{"width":140,"top":89,"left":104,"height":141},"face_token":"ed319e807e039ae669a4d1af0922a0c8"}]
     */

    var image_id: String? = null
    var request_id: String? = null
    var time_used: Int = 0
    var faces: List<FacesBean>? = null

    class FacesBean {
        /**
         * landmark : {"mouth_upper_lip_left_contour2":{"y":185,"x":146},"contour_chin":{"y":231,"x":137},"right_eye_pupil":{"y":146,"x":205},"mouth_upper_lip_bottom":{"y":195,"x":159}}
         * attributes : {"gender":{"value":"Female"},"age":{"value":21},"glass":{"value":"None"},"headpose":{"yaw_angle":-26.625063,"pitch_angle":12.921974,"roll_angle":22.814377},"smile":{"threshold":30.1,"value":2.566890001296997}}
         * face_rectangle : {"width":140,"top":89,"left":104,"height":141}
         * face_token : ed319e807e039ae669a4d1af0922a0c8
         */

        var landmark: LandmarkBean? = null
        var attributes: AttributesBean? = null
        var face_rectangle: FaceRectangleBean? = null
        var face_token: String? = null

        class LandmarkBean {
            /**
             * mouth_upper_lip_left_contour2 : {"y":185,"x":146}
             * contour_chin : {"y":231,"x":137}
             * right_eye_pupil : {"y":146,"x":205}
             * mouth_upper_lip_bottom : {"y":195,"x":159}
             */

            var mouth_upper_lip_left_contour2: MouthUpperLipLeftContour2Bean? = null
            var contour_chin: ContourChinBean? = null
            var right_eye_pupil: RightEyePupilBean? = null
            var mouth_upper_lip_bottom: MouthUpperLipBottomBean? = null

            class MouthUpperLipLeftContour2Bean {
                /**
                 * y : 185
                 * x : 146
                 */

                var y: Int = 0
                var x: Int = 0
            }

            class ContourChinBean {
                /**
                 * y : 231
                 * x : 137
                 */

                var y: Int = 0
                var x: Int = 0
            }

            class RightEyePupilBean {
                /**
                 * y : 146
                 * x : 205
                 */

                var y: Int = 0
                var x: Int = 0
            }

            class MouthUpperLipBottomBean {
                /**
                 * y : 195
                 * x : 159
                 */

                var y: Int = 0
                var x: Int = 0
            }
        }

        class AttributesBean {
            /**
             * gender : {"value":"Female"}
             * age : {"value":21}
             * glass : {"value":"None"}
             * headpose : {"yaw_angle":-26.625063,"pitch_angle":12.921974,"roll_angle":22.814377}
             * smile : {"threshold":30.1,"value":2.566890001296997}
             */

            var gender: GenderBean? = null
            var age: AgeBean? = null
            var glass: GlassBean? = null
            var headpose: HeadposeBean? = null
            var smile: SmileBean? = null
            var beauty: BeautyBean? = null
                private set
            var emotion: EmotionBean? = null

            fun setSmile(beauty: BeautyBean) {
                this.beauty = beauty
            }


            class GenderBean {
                /**
                 * value : Female
                 */

                var value: String? = null
            }

            class AgeBean {
                /**
                 * value : 21
                 */

                var value: Int = 0
            }

            class GlassBean {
                /**
                 * value : None
                 */

                var value: String? = null
            }

            class HeadposeBean {
                /**
                 * yaw_angle : -26.625063
                 * pitch_angle : 12.921974
                 * roll_angle : 22.814377
                 */

                var yaw_angle: Double = 0.toDouble()
                var pitch_angle: Double = 0.toDouble()
                var roll_angle: Double = 0.toDouble()
            }

            class SmileBean {
                /**
                 * threshold : 30.1
                 * value : 2.566890001296997
                 */

                var threshold: Double = 0.toDouble()
                var value: Double = 0.toDouble()
            }

            class BeautyBean {
                /**
                 * value : None
                 */

                var male_score: Double = 0.toDouble()
                var female_score: Double = 0.toDouble()

            }

            class EmotionBean {
                /**
                 * value : None
                 */

                var anger: Double = 0.toDouble()
                var disgust: Double = 0.toDouble()
                var fear: Double = 0.toDouble()
                var happiness: Double = 0.toDouble()
                var neutral: Double = 0.toDouble()
                var sadness: Double = 0.toDouble()
                var surprise: Double = 0.toDouble()
            }
        }

        class FaceRectangleBean {
            /**
             * width : 140
             * top : 89
             * left : 104
             * height : 141
             */

            var width: Int = 0
            var top: Int = 0
            var left: Int = 0
            var height: Int = 0
        }
    }
}
