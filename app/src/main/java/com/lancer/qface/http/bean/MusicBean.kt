package com.lancer.qface.http.bean

/**
 *Time:2019/7/29
 *Author:Lancer
 *Des:
 */
data class MusicBean(
    val album: String,
    val author: String,
    val comment_avatar_url: String,
    val comment_content: String,
    val comment_id: Int,
    val comment_liked_count: Int,
    val comment_nickname: String,
    val comment_pub_date: String,
    val comment_user_id: Int,
    val description: String,
    val images: String,
    val mp3_url: String,
    val pub_date: String,
    val song_id: Int,
    val title: String
)