package com.lancer.qface.http.api


import com.lancer.qface.http.bean.FaceppBean

import com.lancer.qface.http.bean.LyricBean
import com.lancer.qface.http.bean.MusicBean
import io.reactivex.Observable
import retrofit2.http.*

/**
 *Time:2019/7/26
 *Author:Lancer
 *Des:
 */

interface CommonApi {

//https://api.imjad.cn/cloudmusic/?type=lyric&id=1359595520&br=128000
//https://api.comments.hk/


    @GET("/")
    fun getMusic(): Observable<MusicBean>

    @GET("/cloudmusic/")
    fun getLyric(@Query("type") type: String, @Query("id") id: Int, @Query("br") br: Int): Observable<LyricBean>


    //    https://api-cn.faceplusplus.com/
    @POST("facepp/v3/detect")
    @FormUrlEncoded
    fun getFaceInfo(
        @Field("api_key") apikey: String,
        @Field("api_secret") apiSecret: String,
        @Field("image_base64") imageBase64: String,
        @Field("return_landmark") returnLandmark: Int,
        @Field("return_attributes") returnAttributes: String
    ): Observable<FaceppBean>

}