package com.lancer.qface.http;

import com.lancer.qface.BuildConfig;
import com.lancer.qface.http.api.CommonApi;
import com.lancer.qface.http.bean.LyricBean;
import com.lancer.qface.http.bean.MusicBean;
import com.lancer.serviceandreceiver.myhttp.Interceptor.LogInterceptor;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

/**
 * Time:2019/7/29
 * Author:Lancer
 * Des:
 */
public class RetrofitUtils {
    private static final int DEFAULT_TIME_OUT_READ_SECOND = 60; // 默认读取超时时间
    private static final int DEFAULT_TIME_OUT_CONNECT_SECOND = 30; // 默认连接超时时间
    private static CommonApi mCommonApi;

    public RetrofitUtils(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mCommonApi = retrofit.create(CommonApi.class);
    }

    /**
     * 获取一个 OkHttpClient 对象
     *
     * @return OkHttpClient
     */
    public static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        //设置网络连接超时时间及数据读取超时时间
        okHttpBuilder.readTimeout(DEFAULT_TIME_OUT_READ_SECOND, TimeUnit.SECONDS);
        okHttpBuilder.connectTimeout(DEFAULT_TIME_OUT_CONNECT_SECOND, TimeUnit.SECONDS);
        // 设置失败重连
        okHttpBuilder.retryOnConnectionFailure(false);
        // 设置 Log
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // 设置 Log 级别
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpBuilder.interceptors().add(logging);
        okHttpBuilder.addInterceptor(new LogInterceptor());

        // 设置头部信息
//            okHttpBuilder.interceptors().add(new CustomHeaderInterceptor());
        return okHttpBuilder.build();
    }

    /**
     * 封装线程管理和订阅的过程
     *
     * @param observable 被观察者
     * @param observer   观察者
     */
    private  void toSubscribe(Observable observable, Observer observer) {

        observable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public  void Music(Observer<MusicBean> observer) {
        Observable<MusicBean> observable = mCommonApi.getMusic();
        toSubscribe(observable, observer);
    }

    public  void Lyric(Observer<LyricBean> observer, int id) {
        Observable<LyricBean> observable = mCommonApi.getLyric("lyric", id, 128000);
        toSubscribe(observable, observer);
    }


}
