package com.lancer.qface.http;

/**
 * Time:2019/7/29
 * Author:toyk1hz1
 * Des:
 */
public interface onNextWithErrorListener<T> {
    void onNext(T t);
    void onError(Throwable e);
}
