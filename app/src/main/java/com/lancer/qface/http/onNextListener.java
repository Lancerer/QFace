package com.lancer.qface.http;

/**
 * Time:2019/7/29
 * Author:toyk1hz1
 * Des:
 */
public interface onNextListener<T> {
    void onNext(T t);
}
