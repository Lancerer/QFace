package com.lancer.qface.http.api;

import android.util.Log;
import com.lancer.qface.common.Constants;
import com.lancer.qface.http.RetrofitFactory;
import com.lancer.qface.http.base.BaseResponse;
import com.lancer.qface.http.bean.LyricBean;
import com.lancer.qface.http.bean.MusicBean;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class CommonApiService {

    private static final String TAG = CommonApiService.class.getSimpleName();

    private static CommonApiService instance;

    private CommonApi mCommonApi;

    private CommonApiService() {
        Retrofit retrofit = RetrofitFactory.getInstance(Constants.Companion.getBASE_MUSIC_URL()).getRetrofit();
        mCommonApi = retrofit.create(CommonApi.class);
    }

    public static CommonApiService getInstance() {
        if (instance == null) {
            synchronized (CommonApiService.class) {
                if (instance == null) {
                    instance = new CommonApiService();
                }
            }
        }
        return instance;
    }


    /**
     * 封装线程管理和订阅的过程
     *
     * @param observable 被观察者
     * @param observer   观察者
     */
    private void toSubscribe(Observable observable, Observer observer) {

        observable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public void Music(Observer<MusicBean> observer) {
        Observable<MusicBean> observable = mCommonApi.getMusic();
        toSubscribe(observable, observer);
    }

    public void Lyric(Observer<LyricBean> observer, int id) {
        Observable<LyricBean> observable = mCommonApi.getLyric("lyric", id, 128000);
        toSubscribe(observable, observer);
    }


    /**
     * 用来统一处理 Http 的 return 返回码，并将 HttpResponse 的 data 数据剥离出来返回给 Observer
     *
     * @param <T> Subscriber真正需要的数据类型，也就是Data部分的数据类型
     */
    private class HttpResponseFunc<T> implements Function<BaseResponse<T>, BaseResponse<T>> {

        @Override
        public BaseResponse<T> apply(BaseResponse<T> httpResponse) throws Exception {
            // 在此可以统一解析数据，每次请求只需要 HttpResponse.data 数据
            // 但是在此统一处理数据时需要注意返回数据格式的规范性，如果返回数据格式不规范则可能报错
            if (httpResponse != null && httpResponse.getErrorCode() == 0) {
                Log.d("", "httpResponse = " + httpResponse.getErrorCode());
                return httpResponse;
            } else {

            }
            return httpResponse;
        }
    }

}
