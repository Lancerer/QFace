package com.lancer.qface.http.bean

/**
 *Time:2019/7/29
 *Author:Lancer
 *Des:
 */
data class LyricBean(
    val code: Int,
    val klyric: Klyric,
    val lrc: Lrc,
    val qfy: Boolean,
    val sfy: Boolean,
    val sgc: Boolean,
    val tlyric: Tlyric
)

data class Klyric(
    val lyric: Any,
    val version: Int
)

data class Lrc(
    val lyric: String,
    val version: Int
)

data class Tlyric(
    val lyric: Any,
    val version: Int
)