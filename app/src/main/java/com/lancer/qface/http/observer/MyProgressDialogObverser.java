package com.lancer.qface.http.observer;


import android.content.Context;
import com.lancer.qface.http.RxExceptionUtil;
import com.lancer.qface.http.onNextListener;
import com.lancer.qface.http.onNextWithErrorListener;
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MyProgressDialogObverser<T> implements Observer<T> {

    private static final String TAG = MyProgressDialogObverser.class.getSimpleName();
    private onNextListener<T> mTOnNextListener;
    private onNextWithErrorListener mOnNextWithErrorListener;
    private boolean mIsshowing = false;
    private QMUITipDialog mQmuiTipDialog;
    private Disposable d;
    private Context mContext;

    public MyProgressDialogObverser(onNextListener<T> TOnNextListener, Context context) {
        mTOnNextListener = TOnNextListener;
        mContext = context;
    }

    public MyProgressDialogObverser(onNextWithErrorListener onNextWithErrorListener, Context context) {
        mOnNextWithErrorListener = onNextWithErrorListener;
        mContext = context;
    }


    @Override
    public void onSubscribe(Disposable d) {
        this.d = d;
        if (mQmuiTipDialog == null && mIsshowing == false) {
            mQmuiTipDialog = new QMUITipDialog.Builder(mContext)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                    .setTipWord("加载中")
                    .create();
            mQmuiTipDialog.show();
            mIsshowing = true;
        }
    }

    @Override
    public void onNext(T t) {
        if (mTOnNextListener != null) {
            mTOnNextListener.onNext(t);
        }
    }

    @Override
    public void onError(Throwable e) {
        RxExceptionUtil.Companion.exceptionHandler(e);
        hidDialog();

    }

    @Override
    public void onComplete() {
        if (mIsshowing) {
            hidDialog();
        }
    }

    public void hidDialog() {
        if (mQmuiTipDialog != null && mIsshowing == true) {
            mQmuiTipDialog.dismiss();
        }
        mQmuiTipDialog = null;
    }


}
