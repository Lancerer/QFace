package com.lancer.qface.http

/**
 *Time:2019/7/29
 *Author:toyk1hz1
 *Des:
 */
open interface Listener<T> {
    fun onNext(t: T)
}