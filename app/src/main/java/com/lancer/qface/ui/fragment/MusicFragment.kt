package com.lancer.qface.ui.fragment

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.VideoView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.lancer.qface.R
import com.lancer.qface.common.Constants
import com.lancer.qface.http.RetrofitUtils
import com.lancer.qface.http.bean.LyricBean
import com.lancer.qface.http.bean.MusicBean
import com.lancer.qface.http.observer.MyProgressDialogObverser
import com.lancer.qface.http.onNextListener
import kotlinx.android.synthetic.main.fragment_one.*

/**
 *Time:2019/7/29
 *Author:Lancer
 *Des:
 */
class MusicFragment : Fragment() {


    private val mediaPlayer: MediaPlayer = MediaPlayer()
    private var mUrl: String? = null
    private var mAuthor: String? = null
    private var mMusicName: String? = null
    private var mPic: String? = null
    private var ids: Int? = null
    private var flag: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_one, container, false)
        return view
    }


    override fun onResume() {
        super.onResume()
        getMusic()

    }

    private fun getMusic() {

        RetrofitUtils(Constants.BASE_MUSIC_URL)
            .Music(MyProgressDialogObverser<MusicBean>(onNextListener {
                mUrl = it.mp3_url
                Log.d("-----------", it.mp3_url)
                mAuthor = it.author
                mMusicName = it.title
                mPic = it.images
                mMusicAuthorTv.setText(mAuthor)
                mMusicNameTv.setText(mMusicName)
                context?.let {
                    Glide.with(it).load(mPic).apply(RequestOptions().placeholder(R.drawable.main_ic_market))
                        .into(mMusicPicIv)
                }
                ids = it.song_id
                /* context?.let { it1 ->
                     mediaPlayer.setDataSource(
                         it1,
                         Uri.parse("http://m10.music.126.net/20190729205413/f875269f9a65fc05057cb81c25abb3b4/ymusic/67ae/ef87/9484/cbc26350680724387918c5adf1a6393c.mp3")
                     )
                 }
                 mediaPlayer.start()*/
            }, context))


        mMusicNextIv.setOnClickListener {
            getMusic()
        }

        mMusicPicIv.setOnClickListener {
            if (!flag) {
                cl1.visibility = View.INVISIBLE
                cl2.visibility = View.VISIBLE
                flag = false
                ids?.let { it1 ->
                    RetrofitUtils(Constants.BASE_Lyric_URL)
                        .Lyric(MyProgressDialogObverser<LyricBean>(onNextListener {
                            mMusicLyricTv.setText(it.lrc.lyric)
                        }, context), it1)
                }
            }
        }
        cl2.setOnClickListener {
            if (!flag) {
                cl1.visibility = View.VISIBLE
                cl2.visibility = View.INVISIBLE
            }
        }

    }

}
