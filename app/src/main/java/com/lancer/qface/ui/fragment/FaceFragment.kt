package com.lancer.qface.ui.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri

import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.lancer.qface.R
import com.lancer.qface.http.bean.FaceppBean
import com.lancer.qface.utils.Utils
import kotlinx.android.synthetic.main.fragment_two.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 *Time:2019/7/29
 *Author:Lancer
 *Des:
 */
class FaceFragment : Fragment() {
    private val PERMISSIONS_REQUEST_CODE = 1
    private val CAMERA_REQUEST_CODE = 2
    private lateinit var mTmpFile: File
    private lateinit var imageUri: Uri
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        initView()

        return inflater.inflate(R.layout.fragment_two, container, false)
    }

    private fun initView() {
        mPhotoBt.setOnClickListener {
            takePhoto()
        }
    }

    private fun takePhoto() {
        if (Utils.checkAndRequestPermission(context, PERMISSIONS_REQUEST_CODE)) return
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE;
        val path = Environment.getExternalStorageDirectory().absolutePath + "/img";
        if (File(path).exists()) {
            File(path).createNewFile()
        }
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        mTmpFile = File(path, "$fileName.jpg")
        mTmpFile.parentFile.mkdirs()
        val authority = activity!!.packageName + ".provider"
        imageUri = context?.let { FileProvider.getUriForFile(it, authority, mTmpFile) }!!
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            when (requestCode) {

            }
        }
    }


    private fun markFacesInThePhoto(bitmap: Bitmap, faces: List<FaceppBean.FacesBean>): Bitmap {
        val tempBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(tempBitmap)
        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 10F

        for (face in faces) {
            val faceRectangle = face.face_rectangle
            val top = faceRectangle!!.top
            val left = faceRectangle.left
            val height = faceRectangle.height
            val width = faceRectangle.width
            canvas.drawRect(left.toFloat(), top.toFloat(), (left + width).toFloat(), (top + height).toFloat(), paint)
        }
        return tempBitmap
    }


    private fun handleDetectResult(photo: Bitmap, faceppBean: FaceppBean) {
        val faces = faceppBean.faces
        if (faces == null || faces!!.isEmpty()) {
//            mView.displayFaceInfo(null)
        } else {
            val photoMarkedFaces = markFacesInThePhoto(photo, faces!!)
//            mView.displayPhoto(photoMarkedFaces)
//            mView.displayFaceInfo(faces)
        }
    }
}
