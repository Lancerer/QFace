package com.lancer.qface.ui.activity

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.lancer.qface.R
import kotlinx.android.synthetic.main.activity_main.*
import androidx.navigation.fragment.NavHostFragment


class MainActivity : AppCompatActivity() {

    var ids: Int = 1359595520
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //bottom和Navigation结合
        val host = supportFragmentManager.findFragmentById(R.id.mNavHostFragment) as NavHostFragment
        val navController = host.navController
        initBottomView(mBottomView, navController)
    }

    private fun initBottomView(mBottomView: BottomNavigationView, navController: NavController) {
        mBottomView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.page4Fragment -> mCamera.visibility = View.VISIBLE
                else -> mCamera.visibility = View.GONE
            }
        }
    }

    private fun initView() {

    }
}

