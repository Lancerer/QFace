### navigation的使用
其实就是类似于使用Fragment布局里面装多个fragment进行切换，但是就是fragment的返回问题被Navigation托管了

1.添加依赖 navigation<br/>
```
    ext.navigationVersion = "2.0.0"
    implementation "androidx.navigation:navigation-fragment-ktx:$rootProject.navigationVersion"
    implementation "androidx.navigation:navigation-ui-ktx:$rootProject.navigationVersion"
```

2.根据你需要的新建相应数量的Fragment

3.在Res目录下新建navigation目录，在该目录下新建一个navigation的resource文件，
我们把它叫做nav_graph_main.xml

4.在上述的资源文件中做你要做的操作，其中需要注意的新属性api有：
-   app:startDestination="@id/page1Fragment"：表示从id为page1Fragment的fragment开始
-   action android:id="@+id/action_page3" app:destination="@id/nav_graph_page3" />

5.编辑MainActivity，添加主容器
```
<!--app:defaultNavHost="true"表示把back事件交给fragment处理-->
<fragment
            android:id="@+id/mNavHostFragment"
            android:name="androidx.navigation.fragment.NavHostFragment"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1"
            app:defaultNavHost="true"
            app:navGraph="@navigation/nav_graph_main"/>
            
        val host = supportFragmentManager.findFragmentById(R.id.mNavHostFragment) as NavHostFragment
        val navController = host.navController
        initBottomView(mBottomView,navController)

    private fun initBottomView(mBottomView: BottomNavigationView, navController: NavController) {
        mBottomView.setupWithNavController(navController)
    } 
```